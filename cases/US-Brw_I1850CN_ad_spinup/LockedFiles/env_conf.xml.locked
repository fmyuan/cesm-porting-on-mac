<?xml version="1.0"?>

<config_definition>

<!-- ========================================================================== -->
<!--                                                                            -->
<!--      These variables CANNOT be modified once configure -case has been      -->
<!--      invoked without first invoking configure -cleannamelist.              -->
<!--      These variables SHOULD NOT be modified once a run has been submitted. -->
<!--                                                                            -->
<!--      See README/readme_env and README/readme_general for details           -->
<!--                                                                            -->
<!-- ========================================================================== -->



<!--"Run initialization type, valid values: startup,hybrid,branch (char) " -->
<entry id="RUN_TYPE"   value="startup"  />    

<!--"Run start date (yyyy-mm-dd). Only used for startup or hybrid runs (char) " -->
<entry id="RUN_STARTDATE"   value="0001-01-01"  />    

<!--"Reference case for hybrid or branch runs (char) " -->
<entry id="RUN_REFCASE"   value="case.std"  />    

<!--"Reference date for hybrid or branch runs (yyyy-mm-dd) (char) " -->
<entry id="RUN_REFDATE"   value="0001-01-01"  />    

<!--"Reference time of day (seconds) for hybrid or branch runs (sssss) (char) " -->
<entry id="RUN_REFTOD"   value="00000"  />    

<!--"allow same branch casename as reference casename, valid values: TRUE,FALSE (logical) " -->
<entry id="BRNCH_RETAIN_CASENAME"   value="FALSE"  />    

<!--"flag for automatically prestaging the refcase restart dataset, valid values: TRUE,FALSE (logical) " -->
<entry id="GET_REFCASE"   value="FALSE"  />    

<!--"TRUE implies code will be built using mpiserial lib, valid values: TRUE,FALSE (logical) " -->
<entry id="USE_MPISERIAL"   value="TRUE"  />    

<!--"TRUE implies this machine supports the use of the mpiserial lib, valid values: TRUE,FALSE (logical) " -->
<entry id="MPISERIAL_SUPPORT"   value="TRUE"  />    

<!--"base period associated with NCPL coupling frequency, valid values: hour,day,year,decade (char) " -->
<entry id="NCPL_BASE_PERIOD"   value="day"  />    

<!--"number of atm coupling intervals per NCPL_BASE_PERIOD (integer) (char) " -->
<entry id="ATM_NCPL"   value="48"  />    

<!--"number of land coupling intervals per NCPL_BASE_PERIOD (integer) (char) " -->
<entry id="LND_NCPL"   value="$ATM_NCPL"  />    

<!--"number of ice coupling intervals per NCPL_BASE_PERIOD (integer) (char) " -->
<entry id="ICE_NCPL"   value="$ATM_NCPL"  />    

<!--"number of ocn coupling intervals per NCPL_BASE_PERIOD (integer) (char) " -->
<entry id="OCN_NCPL"   value="1"  />    

<!--"number of glc coupling intervals per NCPL_BASE_PERIOD (integer) (char) " -->
<entry id="GLC_NCPL"   value="1"  />    

<!--"Glacier model number of elevation classes, 0 implies no glacier land unit in clm, valid values: 0,1,3,5,10 (integer) " -->
<entry id="GLC_NEC"   value="0"  />    

<!-- ====================================== -->

<!-- ====================================== -->

<!--"atm domain file (char) " -->
<entry id="ATM_DOMAIN_FILE"   value="domain.lnd.${CLM_USRDAT_NAME}_navy.nc"  />    

<!--"path of atm domain file (char) " -->
<entry id="ATM_DOMAIN_PATH"   value="$DIN_LOC_ROOT/share/domains/domain.clm"  />    

<!--"lnd domain file (char) " -->
<entry id="LND_DOMAIN_FILE"   value="domain.lnd.${CLM_USRDAT_NAME}_navy.nc"  />    

<!--"path of lnd domain file (char) " -->
<entry id="LND_DOMAIN_PATH"   value="$DIN_LOC_ROOT/share/domains/domain.clm"  />    

<!--"ice domain file (char) " -->
<entry id="ICE_DOMAIN_FILE"   value="UNSET"  />    

<!--"path of ice domain file (char) " -->
<entry id="ICE_DOMAIN_PATH"   value="$DIN_LOC_ROOT/share/domains"  />    

<!--"prime factors of ice domain grid size (char) " -->
<entry id="ICE_DOMAIN_PRIMEFACS"   value=""  />    

<!--"ocn domain file (char) " -->
<entry id="OCN_DOMAIN_FILE"   value="UNSET"  />    

<!--"path of ocn domain file (char) " -->
<entry id="OCN_DOMAIN_PATH"   value="$DIN_LOC_ROOT/share/domains"  />    

<!--"atm to ocn flux mapping file for fluxes (char) " -->
<entry id="MAP_A2OF_FILE"   value="idmap"  />    

<!--"atm to ocn state mapping file for states (char) " -->
<entry id="MAP_A2OS_FILE"   value="idmap"  />    

<!--"ocn to atm mapping file for fluxes (char) " -->
<entry id="MAP_O2AF_FILE"   value="idmap"  />    

<!--"ocn to atm mapping file for states (char) " -->
<entry id="MAP_O2AS_FILE"   value="idmap"  />    

<!--"atm to land mapping file for fluxes (char) " -->
<entry id="MAP_A2LF_FILE"   value="idmap"  />    

<!--"atm to land mapping file for states (char) " -->
<entry id="MAP_A2LS_FILE"   value="idmap"  />    

<!--"land to atm mapping file for fluxes (char) " -->
<entry id="MAP_L2AF_FILE"   value="idmap"  />    

<!--"land to atm mapping file for states (char) " -->
<entry id="MAP_L2AS_FILE"   value="idmap"  />    

<!--"runoff (.5 degree) to ocn mapping file (char) " -->
<entry id="MAP_R2O_FILE_R05"   value="idmap"  />    

<!--"runoff (19 basin) to ocn mapping file (char) " -->
<entry id="MAP_R2O_FILE_R19"   value="idmap"  />    

<!--"runoff (1 degree) to ocn mapping file (char) " -->
<entry id="MAP_R2O_FILE_RX1"   value="idmap"  />    

<!--"path of mapping files (char) " -->
<entry id="MAP_A2O_PATH"   value="$DIN_LOC_ROOT/cpl/cpl6"  />    

<!--"path of mapping files (char) " -->
<entry id="MAP_O2A_PATH"   value="$DIN_LOC_ROOT/cpl/cpl6"  />    

<!--"path of mapping files (char) " -->
<entry id="MAP_L2A_PATH"   value="$DIN_LOC_ROOT/cpl/cpl6"  />    

<!--"path of mapping files (char) " -->
<entry id="MAP_A2L_PATH"   value="$DIN_LOC_ROOT/cpl/cpl6"  />    

<!--"path of mapping files (char) " -->
<entry id="MAP_R2O_PATH"   value="$DIN_LOC_ROOT/cpl/cpl6"  />    

<!-- ====================================== -->

<!--"Only used for C,G compsets: if ocn, ocn provides EP balance factor for precip, valid values: off,ocn (char) " -->
<entry id="CPL_EPBAL"   value="off"  />    

<!--"Only used for C,G compsets: if true, compute albedos to work with daily avg SW down, valid values: true,false (logical) " -->
<entry id="CPL_ALBAV"   value="false"  />    

<!--"BGC flag, valid values: none,CO2A,CO2B,CO2C,CO2_DMSA (char) " -->
<entry id="CCSM_BGC"   value="none"  />    

<!--"Only used for B,E,F,I compsets: if true will pass VOC fields from lnd to atm, valid values: TRUE,FALSE (char) " -->
<entry id="CCSM_VOC"   value="FALSE"  />    

<!--"CO2 ppmv (real) " -->
<entry id="CCSM_CO2_PPMV"   value="284.7"  />    

<!-- ====================================== -->

<!--"Sets sst/ice_cov stream name for pres runs, only used in F,A compset (char) " -->
<entry id="SSTICE_STREAM"   value="UNSET"  />    

<!--"Sets sst/ice_cov data filename for pres runs, only used in F,A compset (char) " -->
<entry id="SSTICE_DATA_FILENAME"   value="UNSET"  />    

<!--"Sets sst/ice_cov grid filename for pres runs, only used in F,A compset (char) " -->
<entry id="SSTICE_GRID_FILENAME"   value="UNSET"  />    

<!--"Sets model year align of sst/ice_cov for pres runs, only used in F,A compset (integer) " -->
<entry id="SSTICE_YEAR_ALIGN"   value="-999"  />    

<!--"Sets year start of sst/ice_cov for pres runs, only used in F,A compset (integer) " -->
<entry id="SSTICE_YEAR_START"   value="-999"  />    

<!--"Sets year end of sst/ice_cov for pres runs, only used in F,A compset (integer) " -->
<entry id="SSTICE_YEAR_END"   value="-999"  />    

<!-- ====================================== -->

<!--"DATM mode, valid values: CORE2_NYF,CORE2_IAF,TN460,CLM_QIAN,CLM1PT,CPLHIST3HrWx,CLMCRUNCEP (char) " -->
<entry id="DATM_MODE"   value="CLM1PT"  />    

<!--"DATM prescribed aerosol forcing, valid values: none,clim_1850,clim_2000,trans_1850-2000,rcp2.6,rcp4.5,rcp6.0,rcp8.5,pt1_pt1 (char) " -->
<entry id="DATM_PRESAERO"   value="clim_1850"  />    

<!--"year align (only used for CLM_QIAN mode) (integer) " -->
<entry id="DATM_CLMNCEP_YR_ALIGN"   value="1"  />    

<!--"starting year to loop data over (only used for CLM_QIAN mode) (integer) " -->
<entry id="DATM_CLMNCEP_YR_START"   value="1998"  />    

<!--"ending year to loop data over (only used for CLM_QIAN mode) (integer) " -->
<entry id="DATM_CLMNCEP_YR_END"   value="2006"  />    

<!--"case name for coupler history data mode (only used for CPLHIST3HrWx mode) (char) " -->
<entry id="DATM_CPL_CASE"   value="UNSET"  />    

<!--"year align (only used for CPLHIST3HrWx mode) (integer) " -->
<entry id="DATM_CPL_YR_ALIGN"   value="-999"  />    

<!--"starting year to loop data over (only used for CPLHIST3HrWx mode) (integer) " -->
<entry id="DATM_CPL_YR_START"   value="-999"  />    

<!--"ending year to loop data over (only used for CPLHIST3HrWx mode) (integer) " -->
<entry id="DATM_CPL_YR_END"   value="-999"  />    

<!-- ====================================== -->

<!--"CLM configure options (char) " -->
<entry id="CLM_CONFIG_OPTS"   value="-bgc cn -clm4me on -vsoilc_centbgc no-cent"  />    

<!--"CLM build-namelist options (char) " -->
<entry id="CLM_BLDNML_OPTS"   value="-mask navy"  />    

<!--"CLM namelist use_case (char) " -->
<entry id="CLM_NML_USE_CASE"   value="1850_control"  />    

<!--"CLM specific namelist settings (char) " -->
<entry id="CLM_NAMELIST_OPTS"   value=""  />    

<!--"clm co2 type, valid values: constant,diagnostic,prognostic (char) " -->
<entry id="CLM_CO2_TYPE"   value="constant"  />    

<!--"Force CLM model to do a cold-start, valid values: on,off (char) " -->
<entry id="CLM_FORCE_COLDSTART"   value="off"  />    

<!--"My Data ID (char) " -->
<entry id="CLM_USRDAT_NAME"   value="1x1pt_US-Brw"  />    

<!-- ====================================== -->
</config_definition>
