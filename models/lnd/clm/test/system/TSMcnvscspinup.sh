#!/bin/sh 
#

if [ $# -ne 6 ]; then
    echo "TSMcnvscspinup.sh: incorrect number of input arguments" 
    exit 1
fi

test_name=TSMcnvscspinup.$1.$2.$3.$4.$5

if [ -f ${CLM_TESTDIR}/${test_name}/TestStatus ]; then
    if grep -c PASS ${CLM_TESTDIR}/${test_name}/TestStatus > /dev/null; then
        echo "TSMcnvscspinup.sh: CN spinup test has already passed; results are in "
	echo "        ${CLM_TESTDIR}/${test_name}" 
        exit 0
    elif grep -c GEN ${CLM_TESTDIR}/${test_name}/TestStatus > /dev/null; then
        echo "TSMcnvscspinup.sh: test already generated"
    else
	read fail_msg < ${CLM_TESTDIR}/${test_name}/TestStatus
        prev_jobid=${fail_msg#*job}

	if [ $JOBID = $prev_jobid ]; then
            echo "TSMcnvscspinup.sh: CN spinup test has already failed for this job - will not reattempt; "
	    echo "        results are in: ${CLM_TESTDIR}/${test_name}" 
	    exit 2
	else
	    echo "TSMcnvscspinup.sh: this CN spinup test failed under job ${prev_jobid} - moving those results to "
	    echo "        ${CLM_TESTDIR}/${test_name}_FAIL.job$prev_jobid and trying again"
            cp -rp ${CLM_TESTDIR}/${test_name} ${CLM_TESTDIR}/${test_name}_FAIL.job$prev_jobid
        fi
    fi
fi

cfgdir=${CLM_SCRIPTDIR}/../../../../../bld
rundir=${CLM_TESTDIR}/${test_name}
if [ -d ${rundir} ]; then
    rm -r ${rundir}
fi
mkdir -p ${rundir}/timing/checkpoints
if [ $? -ne 0 ]; then
    echo "TSMcnvscspinup.sh: error, unable to create work subdirectory" 
    exit 3
fi
cd ${rundir}

# Configurations
enter="17p_cnvscenterspinupsc_$1"
ad="17p_cnvscadspinupsc_$1"
ad2less="17p_cnvscad2LessADspinupsc_$1"
lessad="17p_cnvsclessADspinupsc_$1"
exitlessad="17p_cnvscexitLessADspinupsc_$1"
normal="17p_cnvscsc_$1"

# Run lengths
enter_spinup_length=1
spinup_length=${6%+*}
ad2lessspinup_length=1
lessadspinup_length=${6#*+}
exitlessadspinup_length=1
normal_length=-1
if [ ${spinup_length} = $6 ] || [ ${lessadspinup_length} = $6 ]; then
    echo "TSMcnvscspinup.sh: error processing input argument for run lengths" 
    echo "FAIL.job${JOBID}" > TestStatus
    exit 4
fi

echo "TSMcnvscspinup.sh: Calling TSM.sh for smoke test of enter-spinup length ${enter_spinup_length}" 
${CLM_SCRIPTDIR}/TSM.sh $enter $2 $3 $4 $5 $enter_spinup_length startup
rc=$?
if [ $rc -ne 0 ]; then
    echo "TSMcnvscspinup.sh: error from TSM.sh= $rc" 
    echo "FAIL.job${JOBID}" > TestStatus
    exit 5
fi

echo "TSMcnvscspinup.sh: Calling TSM.sh for smoke test of AD spinup length ${spinup_length}" 
${CLM_SCRIPTDIR}/TSM.sh $ad $2 $3 $4 $5 $spinup_length continue+$enter.$2.$3.$4.$5.${enter_spinup_length}.startup
rc=$?
if [ $rc -ne 0 ]; then
    echo "TSMcnvscspinup.sh: error from TSM.sh= $rc" 
    echo "FAIL.job${JOBID}" > TestStatus
    exit 5
fi

echo "TSMcnvscspinup.sh: Calling TSM.sh for smoke test of AD to Lesser-AD spinup length ${ad2lessspinup_length}" 
${CLM_SCRIPTDIR}/TSM.sh $ad2less $2 $3 $4 $5 $ad2lessspinup_length continue+$ad.$2.$3.$4.$5.${spinup_length}.continue
rc=$?
if [ $rc -ne 0 ]; then
    echo "TSMcnvscspinup.sh: error from TSM.sh= $rc" 
    echo "FAIL.job${JOBID}" > TestStatus
    exit 5
fi

echo "TSMcnvscspinup.sh: Calling TSM.sh for smoke test of lesser-AD spinup length ${lessadspinup_length}" 
${CLM_SCRIPTDIR}/TSM.sh $lessad $2 $3 $4 $5 $lessadspinup_length continue+$ad2less.$2.$3.$4.$5.${ad2lessspinup_length}.continue
rc=$?
if [ $rc -ne 0 ]; then
    echo "TSMcnvscspinup.sh: error from TSM.sh= $rc" 
    echo "FAIL.job${JOBID}" > TestStatus
    exit 5
fi

echo "TSMcnvscspinup.sh: calling TSM.sh for smoke test of exit-Less-AD length ${exitlessadspinup_length}" 
${CLM_SCRIPTDIR}/TSM.sh $exitlessad $2 $3 $4 $5 ${exitlessadspinup_length} continue+$lessad.$2.$3.$4.$5.${lessadspinup_length}.continue
rc=$?
if [ $rc -ne 0 ]; then
    echo "TSMcnvscspinup.sh: error from TSM.sh= $rc" 
    echo "FAIL.job${JOBID}" > TestStatus
    exit 6
fi

echo "TSMcnvscspinup.sh: calling TSM.sh for smoke test of normal length ${normal_length}" 
${CLM_SCRIPTDIR}/TSM.sh $normal $2 $3 $4 $5 ${normal_length} continue+$exitlessad.$2.$3.$4.$5.${exitlessadspinup_length}.continue
rc=$?
if [ $rc -ne 0 ]; then
    echo "TSMcnvscspinup.sh: error from TSM.sh= $rc" 
    echo "FAIL.job${JOBID}" > TestStatus
    exit 6
fi

exit 0
