<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- Edited by XMLSpy® -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>

<xsl:template match="/">
  <html>
  <body>
  <h2>Supported grids</h2>
    <table border="1">
      <tr bgcolor="#ffffff">
        <th align="left">Grid</th>
        <th align="left">Short Grid Name</th>
        <th align="left">Atm Grid</th>
        <th align="left">Lnd Grid</th>
        <th align="left">OCN Grid</th>
        <th align="left">ICE Grid</th>
        <th align="left">GLC Grid</th>
      </tr>
      <xsl:for-each select="config_horiz_grid/horiz_grid">
      <tr>
        <xsl:if test="count(@GRID) > 0">
          <td align="center"><xsl:value-of select="@GRID"/></td>
          <td align="center"><xsl:value-of select="@SHORTNAME"/></td> 
          <td align="center"><xsl:value-of select="@ATM_GRID"/></td> 
          <td align="center"><xsl:value-of select="@LND_GRID"/></td> 
          <td align="center"><xsl:value-of select="@OCN_GRID"/></td> 
          <td align="center"><xsl:value-of select="@ICE_GRID"/></td> 
          <td align="center"><xsl:value-of select="@GLC_GRID"/></td> 
        </xsl:if>
      </tr>
      </xsl:for-each>
    </table>
    <table border="1">
      <tr bgcolor="#ffffff">
        <th align="left">Grid Name   </th>
        <th align="left" width="40">Number of points in the x direction</th>
        <th align="center">Number of points in the y direction</th>
      </tr>
      <xsl:for-each select="config_horiz_grid/horiz_grid">
      <tr>
        <xsl:if test="@nx > 0">
          <td align="center"><xsl:value-of select="@GLOB_GRID"/></td>
          <td align="center" width="40"><xsl:value-of select="@nx"/></td>
          <td align="center" width="50"><xsl:value-of select="@ny"/></td>
        </xsl:if>
      </tr>
      </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>
