#===============================================================================
# The following always need to be set
# SPMD             Whether to build in SPMD mode or not.  [values TRUE FALSE]
# SMP              Set to TRUE to enable building in SMP mode (uses OpenMP).  
# USER_CPPDEFS     CPP definitions (non platform dependent)
#
#===============================================================================
# Makefile macros for Hera using PGI compilers
#
# Notes:  (see pgi man page & user's guide for the details)
#  -Mextend        => Allow 132-column source lines
#  -Mfixed         => Assume fixed-format source
#  -Mfree          => Assume free-format source
#
#  -byteswapio     => Swap byte-order for unformatted i/o (big/little-endian)
# 
#  -target=linux   => Specifies the target architecture to Hera
#  -fast           => Chooses generally optimal flags for the target platform
#  -Mnovect        => Disables automatic vector pipelining
#  -Mvect=nosse    => Don't generate SSE, SSE2, 3Dnow, and prefetch instructions in loops
#  -Mflushz        => Set SSE to flush-to-zero mode (underflow) loops where possible
#  -Kieee          => Perform fp ops in strict conformance with the IEEE 754 standard. 
#                     Some optimizations disabled, slightly slower, more accurate math. 
#  -mp=nonuma      => Don't use thread/processors affinity (for NUMA architectures)
#
#  -g              => Generate symbolic debug information. Turns off optimization.
#  -gopt           => Generate information for debugger without disabling optimizations
#  -Mbounds        => Add array bounds checking
#  -Ktrap=fp       => Determine IEEE Trap conditions fp => inv,divz,ovf
#                     * inv: invalid operands
#                     * divz divide by zero
#                     * ovf: floating point overflow
#  -Mlist          => Create a listing file
#  -F              => leaves file.f for each preprocessed file.F file
#  -time           => Print execution time for each compiler step
#===============================================================================

# Note that CPPDEFS is set in Macros.cppdefs
CPPDEFS += -DLINUX -DSEQ_$(FRAMEWORK) -DFORTRANUNDERSCORE -DNO_SHR_VMATH 

ifeq ($(compile_threaded), true)
   CPPDEFS += -DTHREADED_OMP
endif

ifeq ($(strip $(MODEL)),cam)
  CPPDEFS += -DNO_R16 -DNO_MPI2
endif

SFC := pgf90
SCC := pgcc
MPIFC := mpipgf90
MPICC := pgcc

# Note that NETCDF_DIR is obtained from invoking modules
NETCDF_PATH   := /usr/local/tools/netcdf-pgi
INC_NETCDF    := $(NETCDF_PATH)/include
LIB_NETCDF    := $(NETCDF_PATH)/lib
MOD_NETCDF    := $(NETCDF_PATH)/include
MPI_PATH := /usr/local/tools/mvapich-pgi/

MPI_LIB_NAME  := 
PNETCDF_PATH  := 
INC_PNETCDF   := 
LIB_PNETCDF   := 
LAPACK_LIBDIR := 

CFLAGS        := -gopt -Mlist
FIXEDFLAGS    := -Mfixed
FREEFLAGS     := -Mfree
FFLAGS        := -i4 -gopt -Mlist -Mextend -byteswapio -Mflushz -Kieee -Ktrap=fp 
FFLAGS_OPT    := 
LDFLAGS       := -L/usr/lib64/ -libverbs -libumad
AR            := ar
MOD_SUFFIX    := mod
CONFIG_SHELL  :=
FPPDEFS := $(CPPDEFS)

#===============================================================================
# Set model and other specific options
# NOTE - all CPPDEFS options must be defined before this point
#===============================================================================

ifeq ($(MODEL),datm)
  CFLAGS     += -O2 -Mnovect
  FFLAGS_OPT += -O2 -Mnovect -Kieee
endif
ifeq ($(MODEL),dice)
  CFLAGS     += -O2 -Mnovect
  FFLAGS_OPT += -O2 -Mnovect -Kieee
endif
ifeq ($(MODEL),dlnd)
  CFLAGS     += -O2 -Mnovect
  FFLAGS_OPT += -O2 -Mnovect -Kieee
endif
ifeq ($(MODEL),docn)
  CFLAGS     += -O2 -Mnovect
  FFLAGS_OPT += -O2 -Mnovect -Kieee
endif

ifeq ($(strip $(MODEL)),cam)
  FFLAGS       := -i4 -gopt -Mlist -Mextend -byteswapio 
  FFLAGS_NOOPT := $(FFLAGS)
  ifneq ($(F_OPTIMIZATION_OVERRIDE),$(null))
     FFLAGS_OPT := $(F_OPTIMIZATION_OVERRIDE)
  else
     CFLAGS     += -O2 -Mvect=nosse 
     FFLAGS_OPT += -O2 -Mvect=nosse -Kieee
  endif
endif
ifeq ($(MODEL),cice)
  CFLAGS     += -O2 -Mvect=nosse 
  FFLAGS_OPT += -O2 -Mvect=nosse -Kieee
endif
ifeq ($(MODEL),clm)
  CFLAGS     += -O2 -Mvect=nosse 
  FFLAGS_OPT += -O2 -Mvect=nosse -Kieee
endif
ifeq ($(MODEL),pop2)
  CFLAGS     += -O2 -Mvect=nosse 
  FFLAGS_OPT += -O2 -Mvect=nosse -Kieee
endif

ifeq ($(DEBUG),TRUE)
   FFLAGS       += -g -Ktrap=fp -Mbounds -Kieee
   FFLAGS_NOOPT += -g -Ktrap=fp -Mbounds -Kieee
else
   FFLAGS += $(FFLAGS_OPT)
endif

ifeq ($(compile_threaded), true)
   FFLAGS       += -mp
   FFLAGS_NOOPT += -mp
   CFLAGS       += -mp
   LDFLAGS      += -mp
endif
