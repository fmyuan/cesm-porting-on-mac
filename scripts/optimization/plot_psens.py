#!/usr/bin/python

import os, sys, csv, glob
import Scientific.IO.NetCDF
from Scientific.IO import NetCDF
from Numeric import *
from optparse import OptionParser
import pp

np  = 30  #number of parameters in sensitivity test
nyr = 20

#ppservers=()
#job_server = pp.Server(ppservers=ppservers)

#print("Starting pp with",job_server.get_ncpus(),"workers")

startyear = 1
vars       = ['GPP', 'NPP', 'FPSN', 'TLAI', 'TOTVEGC', 'BTRAN']
#Energy fluxes
vars       = ['QDRAI', 'QOVER', 'QRUNOFF', 'QSOIL', 'QVEGE', 'QVEGT', \
              'EFLX_LH_TOT', 'FSH']
case      = 'NEWPARMS_US-UMB_I1850CN' 
avpd      =  12

#get parameter names from parms_def.txt
input = open("./parms_def.txt")
pnames=[]
for s in input:
    if (s.split()[3] == "1"):
        pnames.append(s.split()[4])
print(pnames)

#assume default monthly output
thisind=0
output=open('psens_out.txt','w')

nv = len(vars)
sum_psens = zeros([nv,np],Float)
sum_con   = zeros([nv+1],Float)
output    = zeros([nv,np,200000],Float)

var_units=[]
var_long_names=[]

thisoutind=0
for y in range(startyear,startyear+nyr):
    for m in range(1,13):
        print(y,m)
        mst=str(100+m)
        yst=str(10000+y)
        fname_con     = case+'/control/'+case+'.clm2.h0.'+ \
                        yst[1:5]+'-'+mst[1:3]+'.nc'
        nf_con        = NetCDF.NetCDFFile(fname_con,'r')
        for p in range(0,np):
            pst=str(1000+p+1)
            fname_psens=case+'/p'+pst[1:4]+'_001/'+case+'.clm2.h0.'+ \
                         yst[1:5]+'-'+mst[1:3]+'.nc'
            nf_psens    = NetCDF.NetCDFFile(fname_psens,'r')
            for v in range(0,nv):
                myvar_con = nf_con.variables[vars[v]]          
                myvar     = nf_psens.variables[vars[v]]
                #get variable names and units
                if (y == startyear and p == 0):
                    var_long_names.append(myvar.long_name)
                    var_units.append(myvar.units)
                #get control data
                if (p == 10):
                    sum_con[v] = sum_con[v] + float(myvar_con.getValue()[0][0][0])
                #get parameter sensitivity data
                #print(y,p)
                sum_psens[v,p] = sum_psens[v,p] + float(myvar.getValue()[0][0][0])
            nf_psens.close()
        nf_con.close()
        thisind=thisind+1
        if ((thisind % avpd) == 0):
            for v in range(0,nv):
                for p in range(0,np):
                    #output as percentage difference from control
                    if (sum_con[v] > 0):
                        output[v,p,thisoutind]=sum_psens[v,p]/sum_con[v]*100-100.0
                    #print "%.35f" % sum_psens[v,p]
            sum_psens = zeros([nv,np],Float)
            sum_con   = zeros([nv],Float)
            thisoutind=thisoutind+1

#---------- make plots (variables by parameter) -----------------------------
out_plot = open('plotmyvar.p','w')
out_plot.write('set terminal postscript enhanced color\n')
out_plot.write('set output "psens.ps"\n')

for v in range(0,nv):
    #write output variables to be used for plots
    outf = open('psens_'+vars[v]+'.txt','w')
    for i in range(0,thisoutind):
        dst=str(1+(i+0.5)*avpd/12)+' '
        for p in range(0,np):
            dst=dst+str(output[v,p,i])+' '
        outf.write(dst+'\n')
    outf.close()

    #create plotting script
    cmd ='plot "psens_'+vars[v]+'.txt" using 1:2 with lines title "' \
          +pnames[0]+'" linewidth 3, '
    for p in range(1,np):
        cmd = cmd+'"psens_'+vars[v]+'.txt" using 1:'+ \
                   str(p+2)+' with lines linewidth 3 title "'+pnames[p]+'", '
    out_plot.write('set xlabel "model year"\n')
    out_plot.write('set ylabel "'+vars[v]+' difference from control (%)"\n')
    out_plot.write('set title "'+var_long_names[v]+'"\n')
    out_plot.write('set key right outside\n')
    out_plot.write(cmd[:-2]+'\n')

out_plot.close()
os.system('gnuplot plotmyvar.p')
         
            

#---------------- make plots (parameters by variable) -------------------------
out_plot = open('plotmyvar.p','w')
out_plot.write('set terminal postscript enhanced color\n')
out_plot.write('set output "psens_pbyv.ps"\n')

for p in range(0,np):
    #write output variables to be used for plots (variables by parameter)
    outf = open('psens_'+pnames[p]+'.txt','w')
    for i in range(0,thisoutind):
        dst=str(1+(i+0.5)*avpd)+' '
        for v in range(0,nv):
            dst=dst+str(output[v,p,i])+' '
        outf.write(dst+'\n')
    outf.close()

    #create plotting script
    cmd ='plot "psens_'+pnames[p]+'.txt" using 1:2 with lines title "' \
          +vars[0]+'" linewidth 3, '
    for v in range(1,nv):
        cmd = cmd+'"psens_'+pnames[p]+'.txt" using 1:'+ \
                   str(v+2)+' with lines linewidth 3 title "'+vars[v]+'", '
    out_plot.write('set xlabel "model year"\n')
    out_plot.write('set ylabel "difference from control (%)"\n')
    out_plot.write('set title "'+pnames[p]+'"\n')
    out_plot.write('set key right outside\n')
    out_plot.write(cmd[:-2]+'\n')

out_plot.close()
os.system('gnuplot plotmyvar.p')
         
