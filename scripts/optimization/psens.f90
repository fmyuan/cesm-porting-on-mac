module psens_data
  integer npfts, np, np_all
  parameter(np_all= 92)  !number of total parameters
  parameter(np    = 50)  !number of pft-specific parameters
  parameter(npfts = 21)  !number of pfts in pft-physiology file   

  character(len=800) CLM_base, basecase, pftfile_in, optdir
  character(len=15)  parm_names(np_all)
  character(len=6)   mysite

  double precision ccsm_parms(np_all)
  double precision parm_ranges(2,np_all)
  integer useparm(np_all), mypft, lst, npd, startyr, nyears, nyears_sim
  integer n_param, myparams(np_all), nchunks, chunksize
  logical docontinue
  parameter(optdir = '/home/zdr/models/PTCLM_new/testclm4_ornl/clm4_ornl/scripts/optimization')

end module psens_data


program main

  use psens_data

  implicit none

  include 'mpif.h'

  integer I, J, L, N, p, die, myid, numprocs
  integer status(MPI_STATUS_SIZE), ierr, sender, intercomm
  integer errcodes(16), usize, n_assigned_max, n_assigned
  integer nsens, ntasks_tot, ntasks_done, done, ndone
  integer :: root=0
  character(len=800) line, cmd, rundir, fname, pftfile, scratchstr
  character(len=5) yst
  character(len=4) pst, nst, myidst
  double precision ccsm_parms_temp(np_all)
  logical exists, dospawn
  
  nsens = 1   !number of values for parameter sensitivity test
  dospawn  = .false.
  docontinue = .true.

  !get case information, parameter ranges   
  fname=trim(optdir) // '/ccsm_opt.txt'
  call get_clm_info(fname)

  call MPI_INIT( ierr)
  call MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr)
  call MPI_COMM_SIZE( MPI_COMM_WORLD, numprocs, ierr)
  !call MPI_COMM_GET_ATTR(MPI_COMM_WORLD, MPI_UNIVERSE_SIZE, usize, ierr)
  !print*, 'Universe size', usize
  n_assigned_max = 2    

  !go into scratch directory
  write(myidst, '(I4)') 1000+myid
  call system('echo $PBS_SCRATCH > scratch_' // myidst(2:4) // '.txt')
  open(unit=8, file = 'scratch_' // myidst(2:4) // '.txt')
  read(8,'(A800)') scratchstr
  close(8)
  call chdir(trim(scratchstr))
  !call system('rm ' // trim(optdir) // '/scratch_' // myidst(2:4) // '.txt')
  !copy necessary files to scratch directory
  call system('cp -r /home/zdr/models/PTCLM_new/testclm4_ornl/clm4_ornl/scripts/optimization/' &
       // 'observations .')
  call system('cp -r /home/zdr/models/PTCLM_new/testclm4_ornl/clm4_ornl/scripts/optimization/' &
       // 'pftdata .')
  call system('cp /home/zdr/models/PTCLM_new/testclm4_ornl/clm4_ornl/scripts/optimization/' &
       // 'ccsm_opt.txt .')
  call system('cp /home/zdr/models/PTCLM_new/testclm4_ornl/clm4_ornl/scripts/optimization/' &
       // 'parms_def.txt .')
  die=0

 
  if (myid == 0) then 
     !master (assign work)
     ntasks_tot  = n_param*nsens+1
     ntasks_done = 0

     !if we are spawning CCSM as an MPI job
     if (dospawn) then 
        n_assigned=0
        do p=1,n_param
           do n=1,nsens
              !call chdir('/home/zdr/models/PTCLM_new/testclm4_ornl/clm4_ornl/scripts/optimization')
              call run_psens(p, n, .true., 16)
              n_assigned=n_assigned+1
              if (n_assigned == n_assigned_max) then
                 n_assigned=0
                 !wait for assigned tasks to complete
                 call sleep(100)
                 call system('ls -l US-UMR_I1850CN/p00*')
                 call sleep(100)
                 call system('ls -l US-UMR_I1850CN/p00*')
                 call sleep(100)
                 call system('ls -l US-UMR_I1850CN/p00*')
                 call sleep(100)
                 call system('ls -l US-UMR_I1850CN/p00*')
                 stop
              end if
           end do
        end do
     else
        !if we are running CCSM on a single processor (PTCLM)
        !assign first round of tasks to processors
        do i=1,numprocs-1
           n=MOD((i-1),nsens)+1
           p=(i-1)/nsens
           call MPI_SEND(n, 1, MPI_INTEGER, i, i*10000,   MPI_COMM_WORLD, ierr)
           call MPI_SEND(p, 1, MPI_INTEGER, i, i*10000+1, MPI_COMM_WORLD, ierr)
           ntasks_done=ntasks_done+1
           print*, 'MASTER SENT ', i, n, p
           call sleep(1)
        end do

        !run the control simulation on the master
        !call run_psens(0, 1, .false., 0)
 
        !when done, continue assigning tasks until finished
        ndone = 0
        do while (ndone .lt. numprocs) 
           n = MOD((ntasks_done-1), nsens)+1
           p = (ntasks_done-1)/nsens
           call MPI_RECV(done, 1, MPI_INTEGER, MPI_ANY_SOURCE, MPI_ANY_TAG, &
                MPI_COMM_WORLD, status, ierr)
           sender  = status(MPI_SOURCE)
           if (ntasks_done .le. ntasks_tot) then 
              call MPI_SEND(n, 1, MPI_INTEGER, sender, sender*10000,   MPI_COMM_WORLD, ierr)
              call MPI_SEND(p, 1, MPI_INTEGER, sender, sender*10000+1, MPI_COMM_WORLD, ierr)
              ntasks_done = ntasks_done+1
           else
              ndone=ndone+1
           end if
        end do
     end if
  
     call MPI_FINALIZE( ierr )
   
  else
     !I am a slave (do work)
     ccsm_parms_temp = ccsm_parms
     do while (die .eq. 0)
        print*, 'I AM ', myid
        call MPI_RECV(n, 1, MPI_INTEGER, 0, myid*10000,   MPI_COMM_WORLD, status, ierr)
        call MPI_RECV(p, 1, MPI_INTEGER, 0, myid*10000+1, MPI_COMM_WORLD, status, ierr)
        print*, 'I GOT ', myid, n, p

        call run_psens(p, n, .false., 0)
        
        !send "done" tag
        call MPI_SEND(done, 1, MPI_INTEGER, 0, myid, MPI_COMM_WORLD, status, ierr) 
     end do

  end if

end program main


!-------------------------run_psens (perform a parameter sensitivity run) ---------------

subroutine run_psens(p, n, dospawn, procs_tospawn)

  use psens_data

  implicit none

  include 'mpif.h'

  character(len=800) rundir, pftfile, cmd, line
  character(len=4) pst, nst
  integer i, p, n, procs_tospawn, errcodes(procs_tospawn)
  integer ierr, myid, intercomm, nstart
  logical dospawn
  integer :: root=0
  
  !set up run directory and copy files
  !call MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr)
  !print*, 'I AM', myid

  write(pst,'(I4)') 1000+p
  write(nst,'(I4)') 1000+n
        
  !create PFT file with unique identifier
  if (p .eq. 0) then 
     !control run
     pftfile='./pft-physiology_control.nc'
     rundir = trim(basecase) // '/control'
  else
     pftfile='./pft-physiology_' // pst(2:4) // '_' // nst(2:4) // '.nc'
     rundir = trim(basecase) // '/p' // pst(2:4) // '_' // nst(2:4)
  end if

  call system('pwd')
  call setup_rundir(rundir, pftfile)
  call chdir(rundir)

  cmd = 'cp ../../pftdata/' // trim(pftfile_in) // ' ' // trim(pftfile)
  call system(cmd)
  cmd = 'chmod u+w ' // trim(pftfile)
  call system(cmd)

  !modify pft-physiology file
  if (p .ne. 0) call modifyparms(pftfile, myparams(p), 1.05d0)
  
  !call CCSM (first time)
  call system('mkdir -p ' // trim(optdir) // '/' // trim(rundir))

  cmd = './ccsm.exe'
  if (docontinue .eq. .false.) then 

     if (dospawn) then 
        call MPI_COMM_SPAWN(trim(cmd), MPI_ARGV_NULL, procs_tospawn, MPI_INFO_NULL, &
             root, MPI_COMM_WORLD, intercomm, MPI_ERRCODES_IGNORE, ierr)
     else
        call system(cmd)
     end if
     !copy output files back to home area
     call system('mv *.clm2.h0.*.nc ' // trim(optdir) // '/' // trim(rundir))
     !clean up any old restart info
     call system('rm ' // trim(optdir) // '/' // trim(rundir) // '*.r*.*')
     !copy new restart info
     call system('cp *.r*.* ' // trim(optdir) // '/' // trim(rundir))
     call system('cp rpointer.* ' // trim(optdir) // '/' // trim(rundir))
     nstart=2
  else
     nstart=1
  end if
 
  nchunks = nyears_sim/chunksize
  print*, nchunks
  !loop through remaining chunks
  if (nchunks .gt. 1 .or. docontinue .eq. .true.) then 
     do n=nstart,nchunks
        if (n .eq. 1) then 
           !copy restart
           call system('cp ' // trim(optdir) // '/' // trim(rundir) // '/rpointer.* .')
           call system('cp ' // trim(optdir) // '/' // trim(rundir) // '/*.r*.* .')
        end if
        !set up as continue_run in drv_in
        open(8, file = 'drv_in_tmp')
        open(9, file = 'drv_in')
        do i = 1,300
           read(9,'(A800)',end=50) line
           if (i .eq. 9) then 
              write(8,*) "  start_type    = 'continue'"
           else
              write(8,*) trim(line)
           end if
        end do
50      continue
        close(9)
        close(8)
        call system('mv drv_in_tmp drv_in')
        if (dospawn) then 
           call MPI_COMM_SPAWN(trim(cmd), MPI_ARGV_NULL, procs_tospawn, MPI_INFO_NULL, &
                root, MPI_COMM_WORLD, intercomm, MPI_ERRCODES_IGNORE, ierr)
        else
           call system(cmd)
        end if
        !copy output files back to home area
        call system('mv *.clm2.h0.*.nc ' // trim(optdir) // '/' // trim(rundir))
        !call system('mv *.log.* ' // trim(optdir) // '/' // trim(rundir))
        !clean up any old restarts from home area
        call system('rm ' // trim(optdir) // '/' // trim(rundir) // '*.r*.*')
        !copy new restarts
        call system('cp *.r*.* ' // trim(optdir) // '/' // trim(rundir))
        call system('cp rpointer.* ' // trim(optdir) // '/' // trim(rundir))
     end do
  end if

  !clean up files from scratch
  call system('rm *.nc')
  call system('rm *.log.*')
  call system('rm *.exe')
  call system('rm *.nml')
  call system('rm rpointer*')
  call system('rm *.bin')
  call system('rm *_in')

  call chdir('../..')

end subroutine run_psens
  

!-----------------get_clm_info (get data about case to use) -------------------------------

subroutine get_clm_info(fname)
 use psens_data
 implicit none

 character(len=800) fname, dummy, cmd
 integer i
 !get case information 
 open(8, file=fname)
 read(8,"(13x,A6)") mysite
 read(8,"(13x,I3)") lst
 read(8,"(13x,I2)") npd
 read(8,"(13x,I1)") mypft
 read(8,"(13x,I4)") startyr
 read(8,"(13x,I4)") nyears_sim
 read(8,"(13x,I4)") chunksize
 read(8,"(13x,I1)") nyears
 read(8,"(13x,A800)") CLM_base
 read(8,"(13x,A800)") basecase
 read(8,"(13x,A800)") pftfile_in
 close(8)
  
 !load default ccsm parameters and ranges
 fname = trim(optdir) // '/parms_def.txt'
 open(unit=8, file=fname, status='old')
 read(8,*) dummy
 read(8,*) dummy
 
 n_param=0
 useparm(:)=0
 do i=1,200
    read(8,*,end=10) parm_ranges(1:2,i), ccsm_parms(i), useparm(i), parm_names(i)
    if (useparm(i) .gt. 0) then 
       n_param=n_param+1
       myparams(n_param)=i
    end if
 end do

 10 continue
 close(8)
end subroutine get_clm_info

!-----------------setup_rundir (create run directory and copy files) -----------------------

subroutine setup_rundir(rundir, pftfile)
  !subroutine to copy files to new run directory
  use psens_data
  implicit none

  integer i
  character(len=800) cmd, line, rundir, pftfile

  cmd = 'mkdir -p ./' // rundir
  call system(cmd)
  call chdir(rundir)
  !clean up any old output files
  call system('rm *.nc')
  !copy case directory 
  cmd = "cp -r " // trim(CLM_base) // "/" // "run/" // trim(basecase) // "/run/*.nml ./"
  call system(cmd)
  cmd = "cp -r " // trim(CLM_base) // "/" // "run/" // trim(basecase) // "/run/*_in ./"
  call system(cmd)
  cmd = "cp -r " // trim(CLM_base) // "/" // "run/" // trim(basecase) // "/run/*.exe ./"
  call system(cmd)
  cmd = "cp -r " // trim(CLM_base) // "/" // "run/" // trim(basecase) // "/run/*eam.txt ./"
  call system(cmd)
  cmd = "cp -r " // trim(CLM_base) // "/" // "run/" // trim(basecase) // "/run/rpointer* ./"
  call system(cmd)
  cmd = "cp -r " // trim(CLM_base) // "/" // "run/" // trim(basecase) // "/run/timing ./"
  call system(cmd)
  cmd = "cp -r " // trim(CLM_base) // "/" // "run/" // trim(basecase) // "/run/*.rc ./"
  call system(cmd)
  cmd = "cp -r " // trim(CLM_base) // "/" // "run/" // trim(basecase) // "/run/*.r*.*.nc ./"
  call system(cmd)
  cmd = "cp -r " // trim(CLM_base) // "/" // "run/" // trim(basecase) // "/run/*.bin ./"
  call system(cmd)
  
  !change pft physiology file in lnd_in namelist
  open(8, file="lnd_in", status="OLD")
  open(9, file="lnd_in_tmp")
  
  do i=1,35
     read(8,"(A800)",end=35) line
     if (line(1:8) .eq. " fpftcon") then 
        write(9,*) " fpftcon               = '" // trim(pftfile) // "'"
     else
        write(9,*) trim(line)
     end if
  end do
  close(8)
  close(9)

35 continue
  cmd = 'mv lnd_in_tmp lnd_in'
  call system(cmd)
  !modify logfile output locations  
  call modifylogio_loc('atm')
  call modifylogio_loc('cpl')
  call modifylogio_loc('glc')
  call modifylogio_loc('ice')
  call modifylogio_loc('lnd')
  call modifylogio_loc('ocn')
  
  call chdir('../..')
  
end subroutine setup_rundir

!--------------modifylogio_loc (modify ccsm log file locations)---------------------------

subroutine modifylogio_loc(type)

  use psens_data
  implicit none

  !subroutine to modify log file output location  
  integer i
  character(len=3)   type
  character(len=800) line, cmd
  
  open(8, file=type // "_modelio.nml", status="OLD")
  open(9, file=type // "_modelio.nml_tmp")
  do i=1,5
     read(8,"(A800)") line
     if (line(1:7) .eq. "   diro") then
        write(9,*) '   diro   = "./   "'
     else
        write(9,*) trim(line)
     end if
  end do
  close(8)
  close(9)
  cmd = "mv " // type // "_modelio.nml_tmp " // type // "_modelio.nml"
  call system(cmd)

end subroutine modifylogio_loc
  

!----------------modifyparms (modify pft-physiology file) --------------------------------

subroutine modifyparms(pftfile, pnum, parm_fac)
  !subroutine to manipulate parameters in PFT-physiology file

  use netcdf
  use psens_data
  implicit none

  character(len=800) pftfile
  character(len=15) parm_name
  integer i, NCID, RCODE, varid, pnum
  double precision parm_fac, data(npfts), data1d

  parm_name = parm_names(pnum)
  if (pnum .gt. np) then     !non PFT-specific
     RCODE = NF90_open(trim(pftfile), NF90_WRITE, NCID)
     RCODE = NF90_INQ_VARID(NCID, trim(parm_name), varid)
     
     RCODE = NF90_GET_VAR(NCID, varid, data1d)
     data1d  = data1d*parm_fac
     RCODE = NF90_PUT_VAR(NCID, varid, data1d)
     
     RCODE = NF90_CLOSE(NCID)
  else                        !PFT-specific
     RCODE = NF90_open(trim(pftfile), NF90_WRITE, NCID)
     RCODE = NF90_INQ_VARID(NCID, trim(parm_name), varid)
     
     RCODE = NF90_GET_VAR(NCID, varid, data)
     data(mypft+1)  = data(mypft+1)*parm_fac
     RCODE = NF90_PUT_VAR(NCID, varid, data)
     
     RCODE = NF90_CLOSE(NCID)
  end if
  


end subroutine modifyparms
