:Description: 1D variable saturation flow and thermal process

:=========================== flow mode ========================================
MODE TH

:=========================== discretization ===================================
GRID
  TYPE structured
  ORIGIN 0.d0 0.d0 0.d0
  NXYZ 1 1 10
  DXYZ 
    1.d0 
    1.d0 
    :1.13d0 6.90d-1 4.20d-1 2.74d-1 1.53d-1 9.30d-2 6.75d-2 5.46d-2 2.08d-2 7.1d-3
    7.1d-3 2.08d-2 5.46d-2 6.75d-2 9.30d-2 1.53d-1 2.74d-1 4.20d-1 6.90d-1 1.13d0 
  /
END

:=========================== fluid properties =================================
FLUID_PROPERTY
  DIFFUSION_COEFFICIENT 1.d-9
END

:=========================== regions ==========================================
REGION all
  COORDINATES
    0.d0 0.d0 0.d0
    1.d0 1.d0 2.91d0
  /
END

REGION top
  COORDINATES
    0.d0 0.d0 2.91d0
    1.d0 1.d0 2.91d0
  /
  FACE TOP
END

:=========================== flow conditions ==================================
FLOW_CONDITION initial_flow
  TYPE
    PRESSURE hydrostatic
    TEMPERATURE dirichlet
    :ENTHALPY dirichlet
  /
  INTERPOLATION linear
  DATUM 0.d0 0.d0 0.d0
  PRESSURE 101325.d0
  TEMPERATURE 10.d0
  :ENTHALPY 0.d0
END

FLOW_CONDITION et_flow
  TYPE
    RATE mass_rate
    TEMPERATURE dirichlet
    :ENTHALPY dirichlet
  /
  RATE 0.0d0
  TEMPERATURE 1.d0
  :ENTHALPY 0.d0
END

FLOW_CONDITION groundsurface
  TYPE
    !PRESSURE seepage 
    PRESSURE hydrostatic
    TEMPERATURE neumann
    :ENTHALPY dirichlet
  /
  PRESSURE 101325.d0
  TEMPERATURE 0.d0
  :ENTHALPY 0.d0
END

:=========================== condition couplers ===============================
INITIAL_CONDITION initial_coupler
  FLOW_CONDITION initial_flow
  REGION all
END

SOURCE_SINK clm_et_ss
  FLOW_CONDITION et_flow
  REGION all 
END

BOUNDARY_CONDITION clm_gflux_bc
  FLOW_CONDITION groundsurface 
  REGION top
END

:=========================== stratigraphy couplers ============================
STRATA
  REGION all
  MATERIAL soil1
END

:=========================== material properties ==============================
MATERIAL_PROPERTY soil1
  ID 1
  POROSITY 0.55d0
  TORTUOSITY 0.5d0
  ROCK_DENSITY 2.8E3
  SPECIFIC_HEAT 1E3
  THERMAL_CONDUCTIVITY_DRY 0.5
  THERMAL_CONDUCTIVITY_WET 0.5
  SATURATION_FUNCTION sf1
  PERMEABILITY
    PERM_X 7.918d-11
    PERM_Y 7.918d-11
    PERM_Z 7.918d-13
  /
END

:saturation functions for materials above
SATURATION_FUNCTION sf1
  SATURATION_FUNCTION_TYPE VAN_GENUCHTEN
  RESIDUAL_SATURATION 0.05d0
  LAMBDA 0.2593d0
  ALPHA 1.53391d-04
  MAX_CAPILLARY_PRESSURE 1.d8
END

:=========================== times ============================================
TIME
  FINAL_TIME 365.0d0 d
  INITIAL_TIMESTEP_SIZE 1800.0 s
  MAXIMUM_TIMESTEP_SIZE 3600.0 s
END

:=========================== output options ===================================
OUTPUT 
  PERIODIC TIME 1.0d0 d
  FORMAT HDF5
END

:=========================== mapping files  ============================
MAPPING_FILES                   
  CLM2PF_FLUX_FILE map-clm2pf-1x1x10.dat
  CLM2PF_SOIL_FILE map-clm2pf-1x1x10.dat
  CLM2PF_GFLUX_FILE map-clm2pf-surf-1x1x10.dat
  PF2CLM_FLUX_FILE map-pf2clm-1x1x10.dat
END
